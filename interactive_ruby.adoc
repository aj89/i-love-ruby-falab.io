=== Interactive Ruby

Ruby provides us a easy way to interact with it, this feature is called interactive ruby or irbfootnote:[One may also checkout pry http://pryrepl.org/]. With irb you can type small bits of ruby code in your console and see it get executed. irb is a great tool to check out small pieces of Ruby code. In your terminal type `irb` or `irb –-simple-prompt` , you will be getting  prompt as shown

[source, ruby]
----
2.4.0 :001 >
----

The above prompt will be got if you had typed `irb`

[source, ruby]
----
>>
----

The above prompt will be got if you had typed `irb –-simple-prompt`, in examples from now on I will be using the simple prompt. Lets write our first hello world program, in the prompt type the following (don't type those `>>`)

[source, ruby]
----
>> puts 'Hello World!'
----

When you press enter, you will get output as follows. In Ruby puts is used for printing some thing onto the console.

[source, ruby]
----
Hello World !
=> nil
----

Very well, we have completed our hello world program under a minute. Lets check what is 56 to the power of 31 is

[source, ruby]
----
>> 56**31 
=> 1562531701075863192779448904272185314811647640213651456
----

OOPS! You never thought it would be such a large number, did you? Any way, the `**` is used to find a number raised to the power of another number.

To quit irb and return to normal console or terminal prompt type `quit` or press `Ctrl+c` on keyboard.

