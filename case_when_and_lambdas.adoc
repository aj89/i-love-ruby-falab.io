=== case when and lambdas

In link:code/case_odd_even.rb[case_odd_even.rb] , when we tried to check if a number is odd or even, we gave a verbose program whose scope was limited from numbers from 1 to 10 . We can write the same program as shown

[source, ruby]
----
include::code/case_odd_even_lambda.rb[]
----

Output

----
76 is even
----

To understand the above example you must read Proc, Lambdas and Blocks first. Watch the the code `when -> (n) { n % 2 == 0 }` , in it you are passing a lambda to the `when`, which  when called would return `true` for `n` of value 76 or any even number, `false` if `n` is odd. Thus unlike previous odd and even program, this would work everywhere for all natural numbers from zero to infinite.

