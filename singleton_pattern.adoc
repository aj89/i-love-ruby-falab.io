== Singleton Pattern

Imagine that you have a party in your house and you are expecting about 20 guests, each guest must enter your house and hence a door is needed for each of them, so you start bashing your walls and start making space for 20 doors. Doesn't it sound crazy? Don't you think just one door would serve this purpose?

Lets say that you want to have access to database in your Ruby program. You can write a class to connect to it and return the result. Lets say that your program is making 10 simultaneous connections to the database, then do you think its logical to replicate database user name, password and the querying program to be present in 10 places on your computer RAM? or do you think its efficient to have it in just one place and it can be used by all?

Welcome to Singleton pattern, in it no matter what, a singleton object when created even a million times, it is stored in just one place in your computer RAM, thus saving computer space.

Let's look at a very simple example. Below we have a program called link:code/design_patterns/multiton.rb[multiton.rb]. Type it and execute it.


[source, ruby]
----
include::code/design_patterns/multiton.rb[]
----

Output

----
47002601799540
47002601799360
----

In the above program we are creating two instances of the class `Multiton` and we are querying for its `object_id`. If the both the instances occupy the same location in RAM, then we would have got the same object id both the time, but we see its different, so they occupy two different spaces in the RAM.

Take a look at the program link:code/design_patterns/singleton_example.rb[singleton_example.rb]. Type it and execute it.

[source, ruby]
----
include::code/design_patterns/singleton_example.rb[]
----

Output

----
47257803517040
47257803517040
----

In the above program, we have a class called `SingletonExample` and in it we call this statement `include Singleton`, this makes the class a Singleton. (For this note that in the start of the program we have written `require 'singleton'`, yes Ruby has got its baked in singleton library.) That is, it does not create copies of itself in the computer RAM even if its initialized many times. In fact we cannot call any `new` method on this class. To prove that it just occupies one slot in RAM, we call it twice and print the object id using this statement `puts SingletonExample.instance.object_id`, here we create two instances of the singleton class and it returns the same object id, proving it does not replicate itself in various location in RAM and hence a singleton.

