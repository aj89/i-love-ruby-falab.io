== Preface

Its been a long time since I started writing I Love Ruby. I first projected this book as a toy programming book, but not any more, this book is maturing into something serious. Possibly a book to be read by people who are serious about Ruby, hence this book is undergoing a dramatic change. This book is completely written in Asciidoc. Its been proof read from top to bottom. All its examples are worked for Ruby 2.5, and finally this book is appearing online, epub, pdf and print. I hope you enjoy learning Ruby.

