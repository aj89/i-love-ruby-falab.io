= Design Patterns

include::need_for_design_patterns.adoc[]
include::observer_pattern.adoc[]
include::template_pattern.adoc[]
include::factory_pattern.adoc[]
include::decorator_pattern.adoc[]
include::adapter_pattern.adoc[]
include::singleton_pattern.adoc[]
include::composite_pattern.adoc[]
include::builder_pattern.adoc[]
include::strategy_pattern.adoc[]
