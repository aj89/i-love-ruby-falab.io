== Strategy Pattern

Weather you realize or not you are a good strategist. When you wake up in the morning, you look up for a drink of coffee, when coffee powder is not available you might settle for a tea or hot chocolate or milk. You drive to work or school, if a way is blocked you take the other way. At your work you need to think the way you need to present to your boss, weather your boss is in a good mood or not so that you can speak about fact or mask it up. Strategy is everywhere, you just don't notice it. You keep adopting new strategy every time depending on the environmentfootnote:[Some people like Alexander get to kill others using their strategy, and so they become great.].

In the world of computing if we can tell a piece of code to run different algorithms on the fly depending on the flag / parameters passed, then this type of coding is called strategy pattern. Lets take a look at the example link:code/design_patterns/strategy_pattern.rb[strategy_pattern.rb] below:

[source, ruby, linenums]
----
include::code/design_patterns/strategy_pattern.rb[]
----

Type it and execute it

Output

----
News As HTML:

<ul>
<li>You are reading I Love Ruby.</li>
<li>There is water down below in the oceans.</li>
<li>There is air up above our heads.</li>
<li>Even people in space report their is air up above their heads.</li>
<li>Even bald people have air up above their heads.</li>
</ul>


News As Text:


* You are reading I Love Ruby.
* There is water down below in the oceans.
* There is air up above our heads.
* Even people in space report their is air up above their heads.
* Even bald people have air up above their heads.


----

Lets now analyze the code. Look at these lines (29-35):

[source, ruby]
----
news = [
  "You are reading I Love Ruby.",
  "There is water down below in the oceans.",
  "There is air up above our heads.",
  "Even people in space report their is air up above their heads.",
  "Even bald people have air up above their heads."
]
----

Here we declare an array called `news`, and fill it with news items.

Then in line 39, we have this statement `puts NewsGenerator.generate(news, NewsFormatter::HTML)`, which prints out this:

----
<ul>
<li>You are reading I Love Ruby.</li>
<li>There is water down below in the oceans.</li>
<li>There is air up above our heads.</li>
<li>Even people in space report their is air up above their heads.</li>
<li>Even bald people have air up above their heads.</li>
</ul>
----

Notice that to the `generate` method in `NewsGenerator` class, we gives two arguments as input, the first one is the data, in this case the data is the `news` array which contains list of news. Next argument is `NewsFormatter::HTML` which is the strategy that is to be followed for printing.

Now look at generate method in line 24, it looks like this:

[source, ruby]
----
def self.generate(data, formatter)
  formatter.format(data)
end
----

Here we receive the `formatter` as the second argument, that is in this case its `NewsFormatter::HTML`, and we call `format` method on it passing `data` into it as argument. So the program control goes to line 12, that is inside module `NewsFormatter` and into the method `self.format` in class `HTML`, tou can see the piece of code here

[source, ruby]
----
module NewsFormatter
  ....

  class HTML
    def self.format(data)
      html = []
      html << "<ul>"
      data.each { |datum| html << "<li>#{datum}</li>" }
      html << "</ul>"
      html.join "\n"
    end
  end
end
----

In that function we magically transform it into HTML unordered listfootnote:[https://www.w3schools.com/HTML/html_lists.asp], and return it out.

The same stuff happens when we call `puts NewsGenerator.generate(news, NewsFormatter::Text)`, but since we passed different strategy `NewsFormatter::Text`, this piece of code gets executed:

[source, ruby]
----
module NewsFormatter
  class Text
    def self.format(data)
      seperator = "\n* "
      seperator + data.join(seperator)
    end
  end

  ....
end
----


That is function `self.format` in class `Text` in module `NewsFormatter` gets called, this one produces a plain text output like bullet points, and hence you see this output:

----
* You are reading I Love Ruby.
* There is water down below in the oceans.
* There is air up above our heads.
* Even people in space report their is air up above their heads.
* Even bald people have air up above their heads.
----

