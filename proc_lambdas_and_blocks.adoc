== Proc, Lambdas and Blocks

If you have know some programming languages, you might have heard about closures. Proc and Blocks are similar kind of thing. You can take a piece of code, stick it in between a `do` `end` block, assign it to a variable. This variable contains the piece of code and can be manipulated like objects and passed around.

Proc is like a function, but its an object. Lets see an example to know what an Proc is. Type in the program link:code/proc.rb[proc.rb] into the text editor and execute it.

[source, ruby]
----
include::code/proc.rb[]
----

This is how the output will look like

----
Hello world!
Hello world!
----

Lets now walkthru the code to understand it. Take a look at the following lines

[source, ruby]
----
say_hello = Proc.new do
  puts "Hello world!"
end
----

In this case you are taking a single Ruby statement `puts “Hello World!”` and putting it between an do and end. You are making this code a Proc by appending `Proc.new` before the `do` (the start of the block). You are assigning the Proc object to a variable named `say_hello`. Now `say_hello` can be thought as something that contains a piece of program.

Now how to call or execute the code? When we need to call the piece of Proc named `say_hello` we write the following command

[source, ruby]
----
say_hello.call
----

In the link:code/proc.rb[proc.rb] we call `say_hello` twice and hence we get two `Hello World!` as output.

=== Passing parameters

Like functions you can pass parameters to a Proc. To see how it works, type the program link:code/proc_hello_you.rb[proc_hello_you.rb] and execute it.

[source, ruby]
----
include::code/proc_hello_you.rb[]
----

When executed the program gives the following output.

----
Hello Peter
Hello Quater
----

Take a look at this code

[source, ruby]
----
hello_you = Proc.new do |name|
  puts "Hello #{name}"
end
----

in above program. Notice that we are capturing some thing after the `do` keyword, by giving `do |name|`` we are putting something that's been passed to the `Proc` block into the variable named `name`. This variable can now be used anywhere in the Proc block to do something.

In `puts "Hello #{name}"`, we print `Hello` followed by the passed parameter `name`. So we have written a Proc that can accept some thing passed to it. How do we call and pass something to it? Well, notice the statements after the end, look at the first one, it says

[source, ruby]
----
hello_you.call "Peter"
----

`hello_you.call` calls the Proc code to be executed. To the Proc we pass the string `“Peter”`, this string gets copied in to the variable `name` in the Proc and hence we get the output `Hello Peter`.

In a similar way when Ruby interpreter comes across `hello_you.call "Quarter"`, it prints `Hello Quater`.

=== Passing Proc to methods

Just like any object, we can pass Proc to methods. Take a look at the code below (link:code/proc_to_method.rb[proc_to_method.rb]). Study it, code it and execute it

[source, ruby]
----
include::code/proc_to_method.rb[]
----

This is how the output will look like.

----
Hello world!
----

Lets now analyze the code of `execute_proc` function, its code is as follows

[source, ruby]
----
def execute_proc some_proc
  some_proc.call
end
----

We take in one argument called `some_proc` which we assume it as Proc. We then execute it by using its own `call` method, i.e. in function we just call it using `some_proc.call` for the passed Proc to be executed. If you look at next few lines we create a Proc called `say_hello`

[source, ruby]
----
say_hello = Proc.new do
  puts "Hello world!"
end
----

All we do in `say_hello` Proc is to print `Hello world!`. Now we call the method `execute_proc` and pass `say_hello` in the following piece of code

[source, ruby]
----
execute_proc say_hello
----

Having passed the Proc, it gets copied to `some_proc` argument and when `some_proc` is executed or called, it prints `Hello world!` faithfully.

=== Returning Proc from function

I had written earlier that Proc can be treated like object. In fact Proc is an object. We can pass it to functions, which we have seen in the previous section, now we can see an example in which Proc is returned from a function. Take a good look at code given below (link:code/proc_returning_it.rb[proc_returning_it.rb]).

[source, ruby]
----
include::code/proc_returning_it.rb[]
----

When executed, the program throws the following output

----
The length of your name is 15
----

Look at the function `return_proc`. In it we create a new Proc which accepts a parameter called `name`. Assuming that name is a string, we simply print the length of it. Now consider the code that comes after this function which is as follows:

[source, ruby]
----
name_length = return_proc
name_length.call "A.K.Karthikeyan"
----

In the first line `name_length = return_proc`, the `name_length` gets assigned with what ever the `return_proc` method returns. In this case since `Proc.new` is the last statement / block in the `return_proc` method, it returns a new Proc which gets assigned to `name_proc`. So now `name_proc` can accept a parameter. All we do now is to call `name_proc` followed by a `name`

[source, ruby]
----
name_length.call "A.K.Karthikeyan"
----

Hence `name_length` accepts `name` as parameter, calculates its length and prints it. Hence this example shows that its possible to return a Proc from a function.

=== Proc and Arrays

Lets now see how Proc can be used with Arrays to filter them. Take a look at the program link:code/proc_and_array.rb[proc_and_array.rb] below. In it we have declared a Proc called `get_proc` which takes one argument `num`. In the Proc we return `num` if its even which is ensured by the following statement `num unless num % 2 == 0`. Run the program and note the output.

[source, ruby]
----
include::code/proc_and_array.rb[]
----

Output

----
[1, nil, 3, nil, 5, nil, 7, nil]
[1, 3, 5, 7]
[1, nil, 3, nil, 5, nil, 7, nil]
----

Now lets consider the following three statements

[source, ruby]
----
p numbers.collect(&get_odd)
p numbers.select(&get_odd)
p numbers.map(&get_odd)
----

In it, lets just consider the first line `p numbers.collect(&get_odd)`, so we are having a array of numbers stored in variable called `numbers`, we call `collect` on `numbers`, to it we pass the argument `&get_odd`. That `&<name_of_proc>` will call the Proc for each and every element in the array, what ever thats returned by Proc will be collected into a new array. The `p` ensures that the values get printed out for us to verify.

If you observe closely both `p numbers.collect(&get_odd)` and `p numbers.map(&get_odd)` returns arrays with `nil` values in them whereas select filters out the `nil` and returns what that remains.

==== Lambda

Lambdas are just like Procs. There is almost no difference expect two. I will explain one of them here and the other one will be explained in <<The second difference>> section.

Okay lets see a small program now

[source, ruby]
----
include::code/lambda.rb[]
----

Output

----
Hello World!
----

To know how this program works, read Proc, Lambdas and Blocks.

=== Passing Argument to Lambda

Execute the program below.

[source, ruby]
----
include::code/lambda_passing_argument.rb[]
----

Output

----
7 is odd
8 is even
----

To know how it works checkout <<Passing parameters>> section in this chapter.

=== Proc and Lambdas with Functions

Okay, so whats the difference between Proc and Lambda. There are two main differences between them, here is the first one. In the example below  link:code/calling_proc_and_lambda_in_function.rb[calling_proc_and_lambda_in_function.rb] we have two functions namely `calling_lambda` and `calling_proc`, type and run this file on your machine

[source, ruby]
----
include::code/calling_proc_and_lambda_in_function.rb[]
----

Output

----
Started calling_lambda
In Lambda
Finished calling_lambda function
Started calling_proc
----

You will see an output as shown above. So lets walkthru its execution. When `calling_lambda` function is called first the program prints `Started calling_lambda` by executing `puts "Started calling_lambda"`. Next we define a new lambda `some_lambda` and call it using these lines of code

[source, ruby]
----
some_lambda = lambda{ return "In Lambda" }
puts some_lambda.call
----

So when `some_lambda` is called, it returns `"In Lambda”` which gets printed in line `puts some_lambda.call`.

And then the final statement in the function `puts "Finished calling_lambda function"` is executed giving us the following output

----
Started calling_lambda
In Lambda
Finished calling_lambda function
----

when the function finishes.

Next we call the function `calling_proc`, we expect it to behave like `call_lambda`, but it does not! So what happens? All we know from the output is that `puts "Started calling_proc"` gets executed, after that?
Well see the next line  `some_proc = Proc.new { return "In Proc" }`, notice that it has got a return statement. When a Proc is called in a function, and it has a return statement, it terminates that function and returns the value of the return as though the function itself is returning it! Whereas lambda does not do it.

Even if a lambda called in a function and the called lambda has a return statement, it passes the control to next line in the function after it gets called, in a proc call its not so, it simply exits out of the function returning its own value out (as tough the function had returned it).

=== The second difference

In the previous section, I have written about one difference between Proc and Lambda, lets see the second difference here. Look at the code below (executed in irb).

[source, ruby]
----
>> lambda = -> (x) { x.to_s }
=> #<Proc:0x00000001f65b70@(irb):1 (lambda)>
>> lambda.call
ArgumentError: wrong number of arguments (0 for 1)
	from (irb):1:in `block in irb_binding'
	from (irb):2:in `call'
	from (irb):2
	from /home//karthikeyan.ak/.rvm/rubies/ruby-2.1.3/bin/irb:11:in `<main>'
----

I have used irb to demonstrate the example. In the code above we have defined a Lambda in the following statement `lambda = \-> (x) { x.to_s }`, now we then call it using the following statement lambda.call , as you can see since we have a argument `x`, and we are not passing anything to it  the lambda throws an exception and complains about it. Now lets try it for a Proc

[source, ruby]
----
>> proc = Proc.new { |x| x.to_s}
=> #<Proc:0x00000001a17470@(irb):3>
>> proc.call
=> ""
----

So as you can see above if a argument should be passed to a Proc, and if its not passed, the Proc is called without giving a argument, the Proc does not complain, but treats it as `nil`.footnote:[Thanks to weakish for pointing it out https://github.com/mindaslab/ilrx/issues/4]

=== Lambda and Arrays

Execute the program below

[source, ruby]
----
include::code/lambda_and_array.rb[]
----

Output

----
[1, nil, 3, nil, 5, nil, 7, nil]
[1, 3, 5, 7]
[1, nil, 3, nil, 5, nil, 7, nil]
----

To know how it works read <<Proc and Arrays>> section in this chapter.

==== Blocks and Functions

We have seen Procs and how to pass them to methods and functions. Lets now see about Blocks and how to pass them to functions. Type the example  link:code/blocks_in_methods.rb[blocks_in_methods.rb] and execute it

[source, ruby]
----
include::code/blocks_in_methods.rb[]
----

Output

----
[1, 3, 5, 7]
boom thata
----

So lets now see the code. In the code we have defined a method named `some_method` in the following line `def some_method *args, &block`. Notice that we are taking in all arguments in `*args` and we have something new called `&block` that will take in the block of code. You can replace &block with some other variable like `&a` or `&something`, or what ever you prefer.

Right now forget about whats there in the function body. Now lets see the calling of the function, which is shown below

[source, ruby]
----
some_method 1, 3, 5, 7 do
  puts "boom thata"
end
----

So we call `some_method` and pass on arguments `1, 3, 5, 7`. This will be collected in `*args` footnote:[See <<Variable number of arguments>>] variable as an array. Now see that starts with `do` and ends with `end` and in between you can have as many statements as you want, in other words its a block of code. We just have a statement `puts "boom thata"`, and that's is. This block of code will go into the `&block` variable. Now note the following statement in some_method

[source, ruby]
----
def some_method *args, &block
  p args
  block.call
end
----

We call the block using just `block.call` and not `&block.call`, this is important. When we use `call` method on a block the block gets executed and we get the output `“boom thata”` printed out.

Now lets see another example where we can pass a variable to a blocks call. Type in the example below and execute it.

[source, ruby]
----
include::code/blocks_in_methods_1.rb[]
----

Output

----
[1, 3, 5, 7]
boom thata
boom thata
boom thata
boom thata
boom thata
boom thata
boom thata
----

Note that in the `some_method` definition, we have called the Block by using `block.call 7` , where does the number 7 go? Well, see the following lines

[source, ruby]
----
some_method 1, 3, 5, 7 do |number|
  puts "boom thata\n" * number
end
----

After the do we capture the passed variable using `|number|`, so 7 gets stored in `number`. Inside the block we multiply `"boom thata\n"` by `number` and print it out.

