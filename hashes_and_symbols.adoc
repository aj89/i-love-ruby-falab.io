== Hashes and Symbols

Hashes are arrays with index defined by the program or user and not by the Ruby interpreter. Lets see a program to find out how hashes work. Type the following program (link:code/hash.rb[hash.rb]) into your text editor and execute it.

[source, ruby]
----
include::code/hash.rb[]
----

Output 1

----
Enter subject name:Math 
Mark in Math is 70
----

Output 2

----
Enter subject name:French 
Mark in French is 
----

Take a look at output 1. The program asks the user to enter subject name. When the user enters `Math`, the program gives the math mark. Lets walkthru the code. At the very beginning we have the line `mark = Hash.new` , in this line we declare a variable called mark of the type hash. Take a look at the following lines

[source, ruby]
----
mark['English'] = 50
mark['Math'] = 70
mark['Science'] = 75
----

Unlike an array, hash can have a object as index. In this case we have used simple string as index. We put the marks obtained in English, Math and Science in `mark['English']` , `mark['Math']` , `mark['Science']`. The next two statements

[source, ruby]
----
print "Enter subject name:"
sub = gets.chop
----

prompts the user to enter mark, when he does it, it gets stored into the variable called `sub`. In the final line `puts "Mark in #{sub} is #{mark[sub]}"` we simply access the hash value using `sub` as the key and print it out.

Take a look at output 2, in this case I have entered `French` and the program gives out no result. In the following program we will learn how to deal with it.

=== Default values in Hash

When we pass the index to an hash and if its value does exist, then the hash will faithfully return that value. What if the index has no value defined. In the previous example we saw the program returns nothing. In this program we hope to fix it. Look at the this code `mark = Hash.new 0`. In it instead of just giving `mark = Hash.new` as in the previous one, we have given `mark = Hash.new 0` , here the zero is the default value. Now lets run the program and see what happens.

[source, ruby]
----
include::code/hash_default_value.rb[]
----

Output

----
Enter subject name:Chemistry 
Mark in Chemistry is 0 
----

Look at the output, we haven't defined a value for `mark['Chemistry']`, yet when the subject name is specified as Chemistry we get 0 as result. This is so because we have set zero as the default value. So by setting default value we will have a value for those indexes we haven't defined yet.

=== Looping  hashes

Looping in arrays is quiet easy, we normally use each function in array to iterate objects in array. In similar fashion we can loop in hashes. Type the following code hash_looping.rb into a text editor and execute it.

[source, ruby]
----
include::code/hash_looping.rb[]
----

Output

----
Total marks = 195 
----

In the program above we have calculated the total of all marks stored in the Hash `mark`. Note how we use the `each` loop. Note that we get the key value pair by using `|key,value|` in the loop body. The `key` holds the index of the hash and `value` holds the value stored at that particular indexfootnote:[The term index and key refer to the same stuff]. Each time the loop is executed, we add value to total, so at the end the variable total has got the total of the values stored in the hash. At last we print out the total.

Below is another program where we store student marks in a hash, we use the each loop to print the `key` and `value` corresponding to the `key`. Hope you have understood enough to explain the code below all by your self.

[source, ruby]
----
include::code/hash_looping_1.rb[]
----

Output

----
Key => Value 
Science => 75 
English => 50 
Math => 70 
----

=== More way of hash creation

There is another way to create hashes, lets look at it. See below, the explanation of the program is same like the of previous program link:code/hash_looping_1.rb[hash_looping_1.rb] , except for the highlighted line in the program below. I hope you can explain the programs working by yourself.

[source, ruby]
----
include::code/hash_creation_1.rb[]
----

Output

----
Key => Value 
Science => 75 
English => 50 
Math => 70 
----

=== Using symbols

Usually in a hash we use Symbols as keys instead of String. This is because Symbol occupies far less amount of space compared to String. The difference in speed and space requirement may not be evident to you now, but if you are writing a program that creates thousands of hashes it may take a toll. So try to use Symbols instead of String.

So what is symbol? Lets fire up our irb by typing `irb --simple-prompt` in terminal.  In it type the following

[source, ruby]
----
>> :x.class 
=> Symbol 
----

Notice that we have placed a colon before `x` thus making it `:x`. Symbols have a colon at their start. When we ask what class is `:x`, it says its a `Symbol`. A symbol is a thing or object that can be used as a `key` in a hashfootnote:[Well it can be used for many things other than that. For now, remembering this is sufficient.]. In similar way we declare another symbol called `:name` and see what class it belongs.

[source, ruby]
----
>> :name 
=> :name 
>> :name.class 
=> Symbol 
----

A variable can hold a symbol in it. Notice below that we have assigned a variable `a` with value `:apple` which is nothing but a symbol. When we ask what class it is `a` by using `a.class` , it says its a symbol.

[source, ruby]
----
>> a = :apple 
=> :apple 
>> a.class 
=> Symbol 
----

Symbols can be converted to string using the `to_s` method. Look at the irb example below where we convert symbol to string.

[source, ruby]
----
>> :orange.to_s 
=> "orange"
----

To convert a String to Symbol use `.to_sym` method as shown

[source, ruby]
----
>> "hello".to_sym
=> :hello 
----

Let us write a program in which hashes don't use String but Symbols as key. Type in the  program (below) hash_symbol.rb into text editor and execute it.

[source, ruby]
----
include::code/hash_symbol_2.rb[]
----

Output

----
Enter subject name:Math 
Mark in Math is 70 
----

When the program is run, it prompts for subject name, when its entered it shows corresponding mark. Lets walkthru the code and see how it works. Notice that we use Symbols and not Strings as keys in mark Hash as shown

[source, ruby]
----
mark[:English] = 50
mark[:Math] = 70
mark[:Science] = 75
----

Next we prompt the user to enter marks by using print "Enter subject name:" , the user enters the subject name . Now look at the next line, first we get the subject name into  variable `sub` using `gets.chop`. The `chop` removes the enter character `\n` you typed while pressing the enter key in your keyboard. The `to_sym` converts what ever input you have entered to a symbol, all this is finally stored in `sub`.

[source, ruby]
----
sub = gets.chop.to_sym
----

All we do next is to access the hash value which has the key `sub` and print it using the following statement

[source, ruby]
----
puts "Mark in #{sub} is #{mark[sub]}"
----


So you have seen this program link:code/hash_creation_1.rb[hash_creation_1.rb] , we have now knowledge of symbols, so we can write it as follows in


[source, ruby]
----
include::code/hash_creation_1_a.rb[]
----

Output

----
Key => Value 
English => 50 
Math => 70 
Science => 75 
----

See the line `marks = { :English => 50, :Math => 70, :Science => 75 }` we here use symbols instead of strings as a key to hash. Hash has some advantages compared to string as they occupy less memory compared to string (during the runtime of a program).

In ruby 1.9 and beyond there is better way to write link:code/hash_creation_1_a.rb[hash_creation_1_a.rb], you can see it in link:code/hash_creation_2.rb[hash_creation_2.rb] mentioned below. Just look at the this `marks = { English: 50, Math: 70, Science: 75 }` line in program below

[source, ruby]
----
include::code/hash_creation_2.rb[]
----

This program works exactly as the previous one, but the line `marks = { English: 50, Math: 70, Science: 75 }` gets translated (assume that its been translated) to the following code `marks = { :English => 50, :Math => 70, :Science => 75 }` so its the new short form way to declare hash with symbols as key, newly introduced in ruby 1.9 

=== String, frozen string & symbol, their memory foot print

Strings occupy lot of memory lets see an example, fire up your irb and type the following code

[source, ruby]
----
>> c = "able was i ere i saw elba"
=> "able was i ere i saw elba"
>> d = "able was i ere i saw elba"
=> "able was i ere i saw elba"
>> c.object_id
=> 21472860
>> d.object_id
=> 21441620
----

In the above example we see two variables `c` and `d` , both are assigned to the same string "able was i ere i saw elba" , but if I see the object id's by calling on `c.object_id` and `d.object_id` , both are different. This means that the two "able was i ere i saw elba" are stored in different location's. They are duplicated and copied.

This means that lets say you have the same string declared many locations in your program, and all will occupy new memory, so that would cause lot of load on your computer RAM (for large programs).

Now lets see what will happen if we use a new kind of thing called frozen string. Type the code below in irb

[source, ruby]
----
>> a = "able was i ere i saw elba".freeze
=> "able was i ere i saw elba"
>> b = "able was i ere i saw elba".freeze
=> "able was i ere i saw elba"
>> a.object_id
=> 21633340
>> b.object_id
=> 21633340
----

Now in the above example we call a method called `freeze` upon the string. Now when I check `a` and `b`'s object id, both are the same. That means they both point to the same string. They just occupy one space. This is like this. In the previous example new stuff was created every time, but when we assign a variable with a frozen string, it checks  weather the string has already been (frozen) declared, if yes, it simply points to the same location.

Now lets see about how symbols occupy space, weather they duplicate themselves again and again or not. 

[source, ruby]
----
>> e = :some_symbol
=> :some_symbol
>> f = :some_symbol
=> :some_symbol
>> e.object_id
=> 1097628
>> f.object_id
=> 1097628
----

So in the example above we assign `e` and `f` to symbols `:some_symbol` and when we check for their their object id they both are the same, or they both point to the  same location. This means if we declare symbols again and again they won't occupy extra space. Frozen strings and symbols are good for memory. Ordinary strings are bad.

So why am I saying this in hash section? Lets say you see this snippet of code

[source, ruby]
----
>> person = {"name" => "frank"}
=> {"name"=>"frank"}
>> person2 = {"name" => "bob"}
=> {"name"=>"bob"}
and you have this one
>> person = {"name".freeze => "frank"}
=> {"name"=>"frank"}
>> person2 = {"name".freeze => "bob"}
=> {"name"=>"bob"}
----

Just imagine which will occupy lowest amount of memory? Now think about large programs that uses the same structure of hash in tens of lines of code….

=== Compacting Hashes

Just like Arrays can be compacted, this new feature is introduced to Hashes from Ruby 2.4.0 lets the programmer compact it. Lets look at an example in irb or pry

[source, ruby]
----
>> hash = {a: 1, b: nil, c: 2, d: nil, e: 3, f:nil}
=> {:a=>1, :b=>nil, :c=>2, :d=>nil, :e=>3, :f=>nil}
>> hash.compact
=> {:a=>1, :c=>2, :e=>3}
>> hash
=> {:a=>1, :b=>nil, :c=>2, :d=>nil, :e=>3, :f=>nil}
>> hash.compact!
=> {:a=>1, :c=>2, :e=>3}
>> hash
=> {:a=>1, :c=>2, :e=>3}
----

The explanation is very similar to that explained for Array compaction. Please refer it.

=== Transforming hash values

Lets say that you have a Hash object and you want to transform its values. Say you have a hash who values are numbers and you want to convert it into their squares, then use the transform_values function on that instance of Hash. Lets take code and see how it works. Fire up your irb or pry

Lets first define a hash as shown below

[source, ruby]
----
>> hash = {a: 1, b: 2, c: 3}
=> {:a=>1, :b=>2, :c=>3}
----

Now we went to transform the values, so we call the `transform_values` on the hash as shown

[source, ruby]
----
>> hash.transform_values{ |value| value * value }
=> {:a=>1, :b=>4, :c=>9}
----

If you notice, it functions very similar to collect in Array, but it applies to values in a Hash.

Now lets see if hash has changed

[source, ruby]
----
>> hash
=> {:a=>1, :b=>2, :c=>3}
----

The answer is no as you can see from the code snippet above. If you want the Hash instance upon which the `transform_values` is called to be changed, then call it with a bang as shown below. It will change the Hash object that's calling it.

[source, ruby]
----
>> hash.transform_values!{ |value| value * value }
=> {:a=>1, :b=>4, :c=>9}
>> hash
=> {:a=>1, :b=>4, :c=>9}
----

=== Read the rubydoc

Once again, this book can’t explain everything that's in hash. To know more refer rubydocs for hashes here https://ruby-doc.org/core-2.5.1/Hash.html 

