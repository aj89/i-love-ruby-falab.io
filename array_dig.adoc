=== Array dig

Take a look at a simple snippet executed in irb, we have an nested array, lets say that we want to access the string element `“Treasure”`, you can use the dig method as shown

[source, ruby]
----
>> array = [1, 5, [7, 9, 11, ["Treasure"], "Sigma"]]
=> [1, 5, [7, 9, 11, ["Treasure"], "Sigma"]]
>> array.dig(2, 3, 0)
=> "Treasure"
----

