=== until

`while` loop keeps going until a condition becomes `false`, until loop keeps going until a condition becomes `true`. Read the code below, type it in a text editor and execute it.

[source, ruby]
----
include::code/until.rb[]
----

This is what you will get as result

----
1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
----

So how this loop works? At  first we do set `i=1`, then we use the until command and say that until `i` is greater than 10 keep doing some thing in this line `until i>10 do`. What should be done is said between the `do` and `end` key words. So till the condition fails, the code in loops body will be executed, so we get 1 to 10 printed as output.

