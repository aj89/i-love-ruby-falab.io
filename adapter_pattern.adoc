== Adapter Pattern

Adapter pattern is one in which a class can be made to adapt for various needs. Let's say that we have a class called `Animal`, and depending on the way it must adapt, if it needs to behave like a dog, then it needs to say "woof!"; if it needs to behave like a cat, it must say "meow!" and so on. Then we can write a code as shown below:

[source, ruby]
----
include::code/design_patterns/without_adapter.rb[]
----

Output

----
woof!
----

But thinking deep, is that code good? Each time I need to make this animal behave like a new one, I need to change the `Animal` class. What if I had written the `Animal` class in such a way that it can be made to change its behavior without changing its origin l code? Welcome to the Adapter pattern.

Now look at the code link:code/design_patterns/animal.rb[animal.rb] below. Let me not explain it now, but lets skip to its implementation

[source, ruby]
----
include::code/design_patterns/animal.rb[]
----

So in the below program link:code/design_patterns/adapter.rb[adapter.rb], we have used the class `Anaimal` that was written in link:code/design_patterns/animal.rb[animal.rb]

[source, ruby]
----
include::code/design_patterns/adapter.rb[]
----

Output

----
woof!
meow!
----

Here we first call `animal.speak` to which it prints `woof!`, next we tell it to adapt like a cat by commanding `animal.adapter = :cat` and we call `animal.speak` once again and it prints `meow`. Well how this works.

Now lets analyze the code in link:code/design_patterns/animal.rb[animal.rb]. Here we have a class called `Animal` and in it we have got a a function to set the adapter as shown

[source, ruby]
----
def adapter=(adapter)
  @adapter = Animal::Adapter.const_get(adapter.to_s.capitalize)
end
----

When we pass the name of the adapter as a `Symbol`, notice the code `adapter.to_s.capitalize`, say if we pass it as `:cat`, it gets converted to `Cat`, which is nothing but name of a module in `Animal::Adapter`. This gets loaded into the `@adapter` as we get the constant in `Animal::Adapter.const_get(....)`footnote:[https://ruby-doc.org/core-2.1.0/Module.html#method-i-const_get] statement. So we have set the `@adapter`.

Now lets see how this `Animal` class calls the corresponding `speak` method depending on what ever adapter we have set. Lets assume that `@adapter` is loaded with the constant `Animal::Adapter::Cat`, now in function

[source, ruby]
----
def adapter
  return @adapter if @adapter
  self.adapter = :dog
  @adapter
end
----

in `return @adapter if @adapter` the constant gets returned. So in statement `animal.speak` that follows after `animal.adapter = :cat`, it will load the `speak` function in `Animal::Adapter::Cat`. Hence when its called, it returns `meow!`.

Now the advantage of coding class `Animal` in such way is we can extend it to  different animals without modifying the original class. In other words it can be made to adapt to new situations. See the code below in link:code/design_patterns/adapter_2.rb[adapter_2.rb]. We have put a new module named `Owl` in `Animal::Adapter`, and when we set the adapter as owl using `animal.adapter = :cat` and call `animal.speak`, we get the response as `hoo!`

[source, ruby]
----
include::code/design_patterns/adapter_2.rb[]
----

Output

----
hoo!
----

