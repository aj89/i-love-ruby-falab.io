# strategy_pattern.rb

module NewsFormatter
  class Text
    def self.format(data)
      seperator = "\n* "
      seperator + data.join(seperator)
    end
  end

  class HTML
    def self.format(data)
      html = []
      html << "<ul>"
      data.each { |datum| html << "<li>#{datum}</li>" }
      html << "</ul>"
      html.join "\n"
    end
  end
end


class NewsGenerator
  def self.generate(data, formatter)
    formatter.format(data)
  end
end

news = [
  "You are reading I Love Ruby.",
  "There is water down below in the oceans.",
  "There is air up above our heads.",
  "Even people in space report their is air up above their heads.",
  "Even bald people have air up above their heads."
]

puts "News As HTML:"
puts "\n"
puts NewsGenerator.generate(news, NewsFormatter::HTML)
puts "\n\n"

puts "News As Text:"
puts "\n"
puts NewsGenerator.generate(news, NewsFormatter::Text)
puts "\n\n"
