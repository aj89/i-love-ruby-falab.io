# observer.rb

class Person
  attr_accessor :name, :status, :observers

  def initialize name
    @name = name
    @observers =[]
  end

  def set_status status
    @status = status
    notify_observers
  end

  def notify_observers
    for observer in observers
      observer.notify self
    end
  end

  def notify person
    puts "#{person.name}: #{person.status} - notified to #{@name}"
  end
end

vel = Person.new "Vel"
vel.observers << Person.new("Murugan")
vel.observers << Person.new("Swami")

vel.set_status "Hello All!"
