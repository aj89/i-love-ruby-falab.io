== Benchmark

Benchmark is a measure. You measure how long it takes for your code to run. So, why that's important? As you become more serious coder, you finishing a piece of work won't matter much. What matters is how well you write the code and how the code performs in real time environments. You must know to write code that runs fast. To checkout if one snippet of your code is faster than other you can use benchmark.

Take the example below, type it and run it

[source, ruby]
----
include::code/benchmark.rb[]
----

Output
----
       user     system      total        real
+=  55.030000   7.320000  62.350000 ( 62.303848)
<<   0.160000   0.000000   0.160000 (  0.168452)
----

So let me walk through the code, take the line `require 'benchmark'`, the benchmark library is included as part of Ruby standard distribution, so you can require this code without much fuss in  your file.

Now lets look at this block

[source, ruby]
----
Benchmark.bm do|b|

	………….
end
----

What does it do? First we call  function called `bm` in `Benchmark` class and pass a block between `do` and `end`. Now lets see whats in that block

[source, ruby]
----
Benchmark.bm do|b|
  b.report("+= ") do
    a = ""
    1_000_000.times { a += "." }
  end

  b.report("<< ") do
    a = ""
    1_000_000.times { a << "." }
  end
end
----

See the code above. We are preparing a report with `b.report("+= ")`, to the report function we can  pass any string that will be printed in the output. If you look at the output's second line its `+=  55.030000   7.320000  62.350000 ( 62.303848)`, the `+=` is printed because `“+=”` was passed to `b.report()`.

The `b.report()`` opens a block of code to which you can pass anything that needs to be bench-marked. Here we pass a snippet of code shown below

[source, ruby]
----
b.report("+= ") do
  a = ""
  1_000_000.times { a += "." }
end
----

So we are assigning an empty string to a and we are adding something to it a million times using `+=` operator. And we get this

----
       user     system      total        real
+=  55.030000   7.320000  62.350000 ( 62.303848)
----

as output shows it takes total of 62.35 seconds, which is quiet huge. Now lets take a look at the second block

[source, ruby]
----
b.report("<< ") do
  a = ""
  1_000_000.times { a << "." }
end
----

Here we do the same stuff as the first but we use `<<` operator rather than `+=`, this generates the following output as highlighted below

[source, ruby]
----
       user     system      total        real
+=  55.030000   7.320000  62.350000 ( 62.303848)
<<   0.160000   0.000000   0.160000 (  0.168452)
----

So, it takes only 0.1685 seconds to do it with `<<`, so `<<` is far better than `+=` when it comes to string concatenation.

Now lets see some other stuff. You all know that computer has memory. When a program runs, it needs to remember things and it uses up some memory, occasionally when the available memory becomes less, the Ruby interpreter will clean up memory. This is called Garbage Collectionfootnote:[https://en.wikipedia.org/wiki/Garbage_collection_%28computer_science%29]. Its just like your city or municipality collecting garbage so that the city running is normal. Now think what will happen if the garbage is not collected and you encounter garbage that flows onto your streets, the entire city falters, things get really slow. Similarly if a program had run sufficiently long enough its better to collect garbage, other wise the new code that's been run might be slow and if you are bench-marking it, it might show a wrong result.

Now type the program below in text editor and run it.

[source, ruby]
----
include::code/benchmark_2.rb[]
----

Output

----
Testing without cleaning up
       user     system      total        real
+=  0.550000   0.220000   0.770000 (  0.773730)
<<  0.150000   0.010000   0.160000 (  0.159381)

Testing with cleaning up
Rehearsal --------------------------------------
+=   0.520000   0.180000   0.700000 (  0.687914)
<<   0.010000   0.010000   0.020000 (  0.018958)
----------------------------- total: 0.720000sec

         user     system      total        real
+=   0.530000   0.120000   0.650000 (  0.650013)
<<   0.010000   0.000000   0.010000 (  0.015668)
----

If you see the first benchmarks, which are produced by this code

[source, ruby]
----
puts "Testing without cleaning up"
Benchmark.bm do|b|
  b.report("+=") do
    a = ""
    100_000.times { a += "." }
  end

  b.report("<<") do
    a = ""
    1_000_000.times { a << "." }
  end
end
----

In this we use `Benchmark.bm` and run it as usual, it generates the following output:

----
Testing without cleaning up

       user     system      total        real
+=  0.550000   0.220000   0.770000 (  0.773730)
<<  0.150000   0.010000   0.160000 (  0.159381)
----

The benchmark totals 0.77 and 0.16 seconds respectively. After this block we have these lines

[source, ruby]
----
GC.start
puts
----

In these lines, we are collecting the garbage or freeing up the memory that this program had used. Now we run another benchmark which is defined by this code block

[source, ruby]
----
puts "Testing with cleaning up"
Benchmark.bmbm do|b|
  b.report("+=") do
    a = ""
    100_000.times { a += "." }
  end

  b.report("<<") do
    a = ""
    100_000.times { a << "." }
  end
end
----

So what does the `Benchmark.bmbm` do? Till this time we were using `Benchmark.bm`! Well in the above code we have two benchmarks being run. The `bmbm` makes sure that after the first benchmark is done, there is a garbage collection and freeing up of memory so that the next stuff runs in a garbage collected environment so that it has better accurate result. Here is the output generated by second this code

----
Testing with cleaning up

Rehearsal --------------------------------------
+=   0.520000   0.180000   0.700000 (  0.687914)
<<   0.010000   0.010000   0.020000 (  0.018958)
----------------------------- total: 0.720000sec
----

If you can compare the outputs of `100_000.times { a << "." }` without GC and with GC its 0.16 seconds, 0.02 seconds respectively. Now I think you would appreciate the need of garbage collection, be it in a city or in programming.

