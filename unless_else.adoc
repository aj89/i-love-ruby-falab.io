=== unless else

Just like `if` with `else`, we can have `else` in `unless` statement. Type in the program below and execute it

[source, ruby]
----
include::code/unless_1.rb[]
----

This is what  you get when you execute it

----
Enter your age:37 
"You are a grown up"
----

OK, here is how it works , you get your age, convert it into integer in `age = gets.to_i` and store it in a variable called `age`. Concentrate on this piece of code:

[source, ruby]
----
unless age >= 18
  p "You are a minor"
  else p "You are a grown up"
end
----

unless the `age` is less than 18 `“You are a minor”` doesn't get printed out. If the age is greater than or equal to 18 it gets routed to the `else` statement and `“You are a grown up”` gets printed. Note that if we use `else` with `unless` we must terminate the `unless` block with an `end` command.

Lets now look at another program that uses `unless else`. We want to hire people for armed forces, the person should be between 18 and 35 years of age, our program asks the details from a person who wishes to enroll, it checks his age and tells the result. Type the program below and execute it

[source, ruby]
----
include::code/unless_2.rb[]
----

When executed this will be the result

----
Enter your age:23 
"You can enter Armed forces"
----

I think you can explain this program on your own. If else contact me, I will write an explanation unless I am lazy.

