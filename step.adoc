=== step

`step` loop can be thought as combination of `upto` and `downto` all packed in one, execute the code shown below

[source, ruby]
----
include::code/step_1.rb[]
----

and here is the result. This is very similar to `upto`! Don't you see!!

----
1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
----

Now lets modify the program as shown below and save it in another name

[source, ruby]
----
include::code/step_2.rb[]
----

When executed this program produces no output. What have we done wrong? Modify the program as shown below and run it

[source, ruby]
----
include::code/step_3.rb[]
----

Well here is the output of the program

----
10, 9, 8, 7, 6, 5, 4, 3, 2, 1,
----

What goes on in step?  step receives three inputs, consider the code shown below

----
10.step 1, -1
----

The first one is the number that calls step is taken as the initial number, in the above case it is `10`. Next is the ending number in this case it is `1`, that is this function counts from 10 to 1, we must descend in this case, so the count must be in steps of `-1`.

I can modify the same program to print even numbers in 10 to 1 as shown

[source, ruby]
----
include::code/step_4.rb[]
----

This program prints the following output

----
“Even numbers between 10 and 1:”
10, 8, 6, 4, 2, 
----

Lets now try a program that will print even numbers from 1 to 10, this time in ascending order 

[source, ruby]
----
include::code/step_5.rb[]
----

Output

----
“Even numbers between 1 and 10:”
2, 4, 6, 8, 10,
----

In the above code, have started from 2,we will end at 10 and we jump each loop by steps of 2. Inside the loop we simply print the iterating value which is captured in variable `i`.

