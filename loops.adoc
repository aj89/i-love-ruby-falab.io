== Loops

At times you might need to do some  repetitive task lets say that I want to write a rocket countdown program, I want to create a automated robot that count down for rockets, when the count is finished it says “Blast Off”, lets write one and see

[source, ruby]
----
include::code/count_down.rb[]
----

Well I hope you understand the program above. There is one thing I would like to explain, `p` is a short kind of form of `puts`, rather than writing `puts` one can use `p` and get the same resultfootnote:[Well, almost.]. The above program when run prints the following

----
Hello, I am Zigor.... 
I count down for rockets 
10 
9 
8 
7 
6 
5 
4 
3 
2 
1 
"Blast Off!" 
----

So a perfect execution, but we can make this more efficient to code, we will soon see how

include::downto.adoc[]
include::times.adoc[]
include::upto.adoc[]
include::step.adoc[]
include::while.adoc[]
include::until.adoc[]
include::break.adoc[]
include::next.adoc[]
include::redo.adoc[]
include::loop.adoc[]

