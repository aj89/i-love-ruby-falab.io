== Struct and OpenStruct

Okay, in the previous chapters we have seen about classes, now lets see something simple called structfootnote:[Its kind of struct you see in C++, but its much simpler]. Type the following program and execute it

[source, ruby]
----
include::code/struct_start.rb[]
----

Output

----
Hello, I am Karthik, age 30
----

Well, now lets see how it works.  First you are creating a new type of `Struct` using this statements

[source, ruby]
----
Struct.new :name, :age
----

Now you want to name it, so that you can use it, lets name it as `person`

[source, ruby]
----
person = Struct.new :name, :age
----

Once named, this variable `person` will act like a `class`, you can declare a new instance of it like this

[source, ruby]
----
p = person.new
----

In the above statement `p` is the instance of person.

Now we can assign `:name` and `:age` of `p` using the following statements

[source, ruby]
----
p.name = "Karthik"
p.age = 30
----

Then you can print the data like shown below

[source, ruby]
----
puts "Hello, I am #{p.name}, age #{p.age}"
----

That's it. Without using a `class`, you have created a data structure and used it! Don't you think its great?

Its not that `person` in `person = Struct.new :name, :age` should be variable (i.e start with lower case), but it could also be a constant like `Person`. That's what exactly is going on in the next piece of code here

[source, ruby]
----
include::code/struct_constant.rb[]
----

Output

----
Hello, I am Karthik, age 30
----

So in these lines

[source, ruby]
----
Person = Struct.new :name, :age
p = Person.new
----

we have used `Person` with capital P and the code works!

If you are worried about the fact that you need to typed a lot in the previous program you can shorten it as shown below. Just take a look at the code below.

[source, ruby]
----
include::code/struct_one_line.rb[]
----

Output

----
Hello, I am Karthik, age 30
----

We get the same output but in this one line

[source, ruby]
----
p = person.new "Karthik", 30
----

We have managed to eliminate these two lines

[source, ruby]
----
p.name = "Karthik"
p.age = 30
----

If you have noticed it right, doesn't `p = person.new "Karthik", 30` look like a constructor stuff in classes?

Its not that a `Struct` is just limited to its attribute data structure. You can have function that a `Struct` instance could call as shown in below program. Type it and execute it.

[source, ruby]
----
include:code/struct_about_me.rb[]
----

Output

----
Hello, I am Karthik, age 30
----

As you can see, there is a function called `about_me` defined between the `do end` block of the Struct. We declare a person `p` in this line `p = person.new "Karthik", 30`  and call the `about_me` function on `p` like this `puts p.about_me` and the program works fine. You must also note that we can pass arguments to functions in struct, but I haven't shown that example due to my laziness.

Now lets see how to do structure in a wrong way. Type the program below and execute

[source, ruby]
----
include::code/struct_wrong.rb[]
----

Output

----
struct_wrong.rb:7:in `<main>': undefined method `profession=' for #<struct name="Karthik", age=30> (NoMethodError)
----

If you get the kind of output as shown above, it means that you have typed the program rightly wrong. The problem is in the line `p.profession = "Engineer"`, we are assigning data to  a attribute named profession which we haven't declared in the struct `person = Struct.new :name, :age`. So it throws an error.  To avoid these kind of things, you can use a Open Struct as shown in program below

[source, ruby]
----
include::code/open_struct.rb[]
----

Output

----
Hello, I am Karthik, age 30
----

Open Struct is like Struct, but its does not have its data structure or attributes predefined.
