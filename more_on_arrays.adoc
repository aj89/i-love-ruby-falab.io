=== More on Array

Lets now see some array functions. For this we will be using our favorite irb rather than a text editor

[source, ruby]
----
>> array = Array.new
=> []
----

OK in the above statement we see that we create an Array named `array` using `Array.new`. `Array.new` creates an empty array.
There is another way to create an array. We can create it by directly specifying the values that are contained in an array as shown

[source, ruby]
----
>> array = ["Something", 123, Time.now]
=> ["Something", 123, Tue Feb 02 20:30:41 +0530 2010]
----

In the above statement, we create an array with three objects in it. The value that must be in an array is given between square brackets `[` and `]`. Each object in array is separated by a comma. By providing no values between `[` and `]` we can even create an empty array as shown

[source, ruby]
----
>> array = []
=> []
----

In the above example the empty `[]` does the same job as Array.new .

Lets create array with parameters in `Array.new` as shown

[source, ruby]
----
>> array = Array.new("Something", 123, Time.now)
ArgumentError: wrong number of arguments (3 for 2)
	from (irb):3:in `initialize'
	from (irb):3:in `new'
	from (irb):3
	from :0
----

As you see above it fails! Don't use it that way.

OK, lets now try some thing on the `array`, first to get how many elements are in the `array` we can use the `length` function as shown below:

[source, ruby]
----
>> array.length
=> 3
----

The `join` function joins many array elements together and returns it. So when elements in our array are joined this is what we get as result:

[source, ruby]
----
>> array.join(', ')
=> "Something, 123, Tue Feb 02 20:30:41 +0530 2010"
----

Note that we pass a string `', '` to the `join` when the array elements are joined as a string. The string we passed gets inserted into them in between.

We have created an array and we have something in it, what if we want to add something motr to it? To do so we use the `push` method. In the example below, we push a number 5 into the `array` and as we see the array gets expanded and 5 is appended to the `array` at the last.

[source, ruby]
----
>> array.push(5)
=> ["Something", 123, Tue Feb 02 20:30:41 +0530 2010, 5]
----

The `pop` method does the opposite of `push`, it pops out or removes the last element of array. See the example below, we pop an element and the last element which is 5 gets popped out.

[source, ruby]
----
>> array.pop
=> 5
----

After popping it out lets see whats in the `array`

[source, ruby]
----
>> array
=> ["Something", 123, Tue Feb 02 20:30:41 +0530 2010]
----

We see that the array size has reduced by one and last element 5 is missing.

Its not that you must only give a fixed values in `push`, you can give variables and Ruby expressions and any object to the push as argument. You can see below that we are pushing 2 raised to the power of 10 to the array and 1024 gets added to the array at the last.

[source, ruby]
----
>> array.push 2**10
=> ["Something", 123, Tue Feb 02 20:30:41 +0530 2010, 1024]
----

Array elements are indexed. The first element of an array has a index number 0 and its goes on (theoretically till infinity). If  one wants to access element at an index, all he needs to do is to put the index number in between square brackets. In the example below we  access the  third element of the array named `array` so we type it as follows

[source, ruby]
----
>> array[2]
=> Tue Feb 02 20:30:41 +0530 2010
----

The `pop` method accepts a Integer as an argument which it uses to pop `n` last elements of the array.

[source, ruby]
----
>> array.pop(2)
=> [Tue Feb 02 20:30:41 +0530 2010, 1024]
>> array
=> ["Something", 123]
----

As you see the third element gets popped out, so popping at random is possible.

We can push many elements into an array at once. Consider the code snippet below

[source, ruby]
----
>> array.push 5, "Who am I?", 23.465*24
=> ["Something", 123, 5, "Who am I?", 563.16]
----

We first push 3 new elements into the array and so we get a bigger one.

Now we pop last 3 elements

[source, ruby]
----
>> array.pop 3
=> [5, "Who am I?", 563.16]
----

As you can see the array size is reduced and it now only has two elements.

[source, ruby]
----
>> array
=> ["Something", 123]
----

There is another way to append elements in an array, its by using the double less than operator `<<`, lets push some elements into the array with it as shown:

[source, ruby]
----
>> array << "a new element"
=> ["Something", 123, "a new element"]
>> array << 64
=> ["Something", 123, "a new element", 64]
----

as you see above we have appended a String constant `“a new element”` and `64` to the array using `<<` operator.

You can find maximum and minimum values in an array using the `max` and `min` function as shown:

[source, ruby]
----
>> nums = [1, 2, 64, -17, 5 ,81]
=> [1, 2, 64, -17, 5, 81]
>> nums.max
=> 81
>> nums.min
=> -17
----

As you see in above example we create a array called `nums` having some numbers, `nums.max` returns the maximum value in that array and `nums.min` returns the minimum value in that array.

